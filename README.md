# TP de MDI et de AL

Ici vous retrouverer une partie de ce que j'ai réalisé lors des TP de MDI et de AL. Si vous voulez consulté les Tp qui ont été fait en commun avec les IN c'est ici : 
* design pattern : https://gitlab.com/Diane.S/design-pattern
* docker : https://gitlab.com/Diane.S/tp-docker

J'ai réalisé les Tp en commun avec les IN avec Caroline PINTE et ceux uniques aux SI seule.  

Je me suis permise de vous rajouter le CR qu'on avait fait avec Caroline sur le premier TP qui consistait à découvrir Maven et Jenkins.  

Il y a un dossier JPA qui regroupe le TP4, 5 et 7 vous y retrouver un readme à la racine de chaque expliquant les Tps plus en détails.  
L'autre dossier est le dossier correspondant au TP6 qui était le Tp de réalisation d'un pokedex afin de s'initier à Angular.
  
J'ai réalisé un `swagger`pour le TP5.

N'hésitez pas à me contacter au moindre problème.