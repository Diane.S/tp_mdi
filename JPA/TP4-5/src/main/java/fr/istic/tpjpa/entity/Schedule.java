package fr.istic.tpjpa.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Schedule {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "start")
    private Date start;

    @Column(name = "end")
    private Date end;

    @Column(name = "location")
    private String location;

    @ManyToMany
    @JoinTable(
            name = "schedule_employee",
            joinColumns = @JoinColumn(name = "schedule_id"),
            inverseJoinColumns = @JoinColumn(name = "employee_id")
    )
    private Set<Employee> votes;

    public Schedule() {
        votes = new HashSet<Employee>();
    }

    public Schedule(Date start, Date end) {
        this();
        this.start = start;
        this.end = end;
    }

    public Schedule(Date start, Date end, String location) {
        this(start, end);
        this.location = location;
    }

    public void addVote(Employee employee) {
        votes.add(employee);
    }

    public void removeVote (Employee employee){
        assert(votes.contains(employee));
        votes.remove(employee);
    }

    public int getVotes(){
        return votes.size();
    }

    public Date getStart(){
        return start;
    }
    public Date getEnd(){
        return end;
    }
    public long getId(){
        return id;
    }
    public String getLocation()  {
        return location;
    }

    @Override
    public boolean equals(Object o){
        if(o==this){
            return true;
        }
        if(!(o instanceof Schedule)){
            return false;
        }
        Schedule schedule =(Schedule) o;
        return this.getId()==schedule.getId();
    }

    public Set<Employee> getEmployees(){
        return votes;
    }

}
