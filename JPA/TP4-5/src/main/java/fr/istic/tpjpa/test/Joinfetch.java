package fr.istic.tpjpa.test;


import fr.istic.tpjpa.entity.Department;
import fr.istic.tpjpa.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;

public class Joinfetch {

	private EntityManager manager;

	public Joinfetch(EntityManager manager) {
		this.manager = manager;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("withoutcreate");
		EntityManager manager = factory.createEntityManager();
		fr.istic.tpjpa.test.Joinfetch test = new fr.istic.tpjpa.test.Joinfetch(manager);

		
		TypedQuery<Department> q = test.manager.createQuery("select distinct d from Department d join fetch d.employees e", Department.class);
		long start = System.currentTimeMillis();
		List<Department> res = q.getResultList();
		
		for (Department d : res){
			for (Employee e : d.getEmployees()){
				e.getName();
			}
		}

		long end = System.currentTimeMillis();
		long duree = end - start;
		System.err.println("temps d'exec = " +  duree);

		// TODO persist entity


		// TODO run request

		System.out.println(".. done");
	}

}
