package fr.istic.tpjpa.rest;

import fr.istic.tpjpa.EntityManagerHelper;
import fr.istic.tpjpa.dao.EmployeeDAO;
import fr.istic.tpjpa.dao.MeetingDAO;
import fr.istic.tpjpa.dao.ScheduleDAO;
import fr.istic.tpjpa.entity.Employee;
import fr.istic.tpjpa.entity.Meeting;
import fr.istic.tpjpa.entity.Schedule;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;
import java.util.Set;

@XmlRootElement
class MeetingElement {
    @XmlElement
    public String name;
    @XmlElement
    public String description;
    @XmlElement
    public long organizer;
}

@XmlRootElement
class ScheduleElement {
    @XmlElement
    public Date start;
    @XmlElement
    public Date end;
    @XmlElement
    public String location;
}

@XmlRootElement
class Id {
    @XmlElement
    public long id;
}


@Path("/meetings")
public class MeetingWebService {
    private static MeetingDAO meetingDao = EntityManagerHelper.getMeetingDao();
    private static EmployeeDAO employeeDao = EntityManagerHelper.getEmployeeDao();
    private static ScheduleDAO scheduleDao = EntityManagerHelper.getScheduleDao();

    @GET
    @Path("/{meetingId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Meeting getMeeting(@PathParam("meetingId") long meetingId) {
        return meetingDao.findbyId(meetingId);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Meeting> getAllMeeting() {
        return meetingDao.getAll();
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Meeting addMeeting(MeetingElement meeting) {
        try {
            Employee organizer = employeeDao.findbyId(meeting.organizer);
            Meeting r = new Meeting(meeting.name, meeting.description, organizer);
            meetingDao.save(r);
            return r;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @DELETE
    @Path("/{meetingId}")
    public void deleteMeeting(@PathParam("meetingId") long meetingId) {
        try{
            meetingDao.deleteById(meetingId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @PUT
    @Path("/{meetingId}")
    public Meeting editMeeting() {
        // TODO
        return null;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/participants")
    public Meeting addParticipants(@PathParam("meetingId") long meetingId, Id participantId) {
        try {
            Employee participant = employeeDao.findbyId(participantId.id);
            Meeting meeting = meetingDao.findbyId(meetingId);
            meeting.addParticipants(participant);
            meetingDao.save(meeting);
            return meeting;
        }catch (Exception e){
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/choices")
    public Set<Schedule> addPropositions(@PathParam("meetingId") long meetingId, ScheduleElement element) {
        Schedule schedule = new Schedule(element.start, element.end, element.location);
        try {
            Meeting meeting = meetingDao.findbyId(meetingId);
            meeting.addChoice(schedule);
            meetingDao.save(meeting);
            return meeting.getChoices();
        }catch (Exception e){
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/schedule/{scheduleId}/vote")
    public Meeting addVote(@PathParam("meetingId")long meetingId, @PathParam("scheduleId")long scheduleId, Id employeeId){
        try{
            Meeting meeting = meetingDao.findbyId(meetingId);
            Schedule schedule = scheduleDao.findbyId(scheduleId);
            Employee employee = employeeDao.findbyId(employeeId.id);
            assert (meeting.getChoices().contains(schedule));
            schedule.addVote(employee);
            scheduleDao.save(schedule);
            return meeting;
        }catch (Exception e){
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/schedule/{scheduleId}/vote")
    public Meeting removeVote(@PathParam("meetingId")long meetingId, @PathParam("scheduleId")long scheduleId, Id employeeId){
        try{
            Meeting meeting = meetingDao.findbyId(meetingId);
            Schedule schedule = scheduleDao.findbyId(scheduleId);
            Employee employee = employeeDao.findbyId(employeeId.id);
            assert (meeting.getChoices().contains(schedule));
            schedule.removeVote(employee);
            scheduleDao.save(schedule);
            return meeting;
        }catch (Exception e){
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/participants/{participantId}")
    public Meeting removeParticipant(@PathParam("meetingId") long meetingId, @PathParam("participantId") long participantId) {
        try {
            Meeting meeting = meetingDao.findbyId(meetingId);
            Employee participant= employeeDao.findbyId(participantId);
            meeting.removeParticipants(participant);
            meetingDao.save(meeting);
            return meeting;
        }catch (Exception e){
            e.printStackTrace();
            throw new WebApplicationException(400);
        }

    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/choices/{choiceId}")
    public Set<Schedule> removeChoice(@PathParam("meetingId") long meetingId, @PathParam("choiceId") long choiceId) {
        try{
            Schedule schedule = scheduleDao.findbyId(choiceId);
            Meeting meeting = meetingDao.findbyId(meetingId);
            meeting.removeChoice(schedule);
            scheduleDao.delete(schedule);
            meetingDao.save(meeting);
            return meeting.getChoices();
        }catch (Exception e){
            e.printStackTrace();
            throw new WebApplicationException(400);
        }

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{meetingId}/validate")
    public Meeting validateMeeting(@PathParam("meetingId") long meetingId, Id scheduleId) {
        try {
            Meeting meeting = meetingDao.findbyId(meetingId);
            Schedule schedule = scheduleDao.findbyId(scheduleId.id);
            meeting.validate(schedule);
            meetingDao.save(meeting);
            return meeting;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

}
