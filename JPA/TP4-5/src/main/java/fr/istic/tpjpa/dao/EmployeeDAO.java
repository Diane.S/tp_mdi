package fr.istic.tpjpa.dao;

import fr.istic.tpjpa.entity.Employee;
import fr.istic.tpjpa.EntityManagerHelper;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class EmployeeDAO extends Dao<Employee> {

    public EmployeeDAO(EntityManager manager) {
        super(manager, Employee.class);

    }

    public Employee findbyemail(String email) {
        TypedQuery<Employee> q = EntityManagerHelper.getEntityManager()
                .createQuery("SELECT employee FROM Employee employee WHERE employee.email = :email", Employee.class);
        q.setParameter("email", email);
        return q.getSingleResult();
    }

    public List<Employee> getAll() {
        Query query = manager.createQuery("select employee from Employee employee", Employee.class);
        return query.getResultList();
    }



}
