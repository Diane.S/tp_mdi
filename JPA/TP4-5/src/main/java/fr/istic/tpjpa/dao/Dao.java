package fr.istic.tpjpa.dao;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

public abstract class Dao<T> {
    final Class<T> typeParameterClass;

    protected EntityManager manager;

    public Dao(EntityManager manager, Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
        this.manager = manager;
    }
    /**
     * Find <T> based on the entity Id.
     *
     * @param id the <T> Id.
     * @return <T>.
     * @throws EntityNotFoundException when no T is found.
     */
    public T findbyId(Long id){
        T t = manager.find(typeParameterClass, id);
        if (t == null) {
            throw new EntityNotFoundException("Can't find "+typeParameterClass.getName()+" for ID "
                    + id);
        }
        return t;
    }

    /**
     * Persist a <T> on the database
     * @param t <T>
     */
    public void save(T t) {
        manager.getTransaction().begin();
        manager.persist(t);
        manager.getTransaction().commit();
    }

    /**
     * Update <T> information.
     *
     * @param t an <T> to be updated.
     */
    public void update(T t) {
        manager.getTransaction().begin();
        manager.merge(t);
        manager.getTransaction().commit();
    }

    /**
     * Delete <T> by their Id.
     *
     * @param id the <T> Id.
     */
    public void deleteById(Long id) {
        T t = manager.find(typeParameterClass, id);
        if (t != null) {
            manager.getTransaction().begin();
            manager.remove(t);
            manager.getTransaction().commit();
        }
    }

    /**
     * Delete <T> entity.
     *
     * @param t the object to be deleted.
     */
    public void delete(T t) {
        manager.getTransaction().begin();
        manager.remove(t);
        manager.getTransaction().commit();
    }
}
