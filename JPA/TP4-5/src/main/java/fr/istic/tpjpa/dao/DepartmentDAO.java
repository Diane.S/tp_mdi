package fr.istic.tpjpa.dao;

import fr.istic.tpjpa.EntityManagerHelper;
import fr.istic.tpjpa.entity.Department;
import fr.istic.tpjpa.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class DepartmentDAO extends Dao<Department> {
    public DepartmentDAO(EntityManager manager){
        super(manager, Department.class);
    }

    public Department findbyname(String name) {
        TypedQuery<Department> q = EntityManagerHelper.getEntityManager()
                .createQuery("SELECT department FROM Department department WHERE department.name = :name", Department.class);
        q.setParameter("name", name);
        return q.getSingleResult();
    }

    public List<Department> getAll() {
        Query query = manager.createQuery("select department from Department department", Department.class);
        return query.getResultList();
    }
}
