package fr.istic.tpjpa.test;

import fr.istic.tpjpa.EntityManagerHelper;
import fr.istic.tpjpa.dao.DepartmentDAO;
import fr.istic.tpjpa.dao.EmployeeDAO;
import fr.istic.tpjpa.dao.MeetingDAO;
import fr.istic.tpjpa.dao.ScheduleDAO;
import fr.istic.tpjpa.entity.Department;
import fr.istic.tpjpa.entity.Employee;
import fr.istic.tpjpa.entity.Meeting;
import fr.istic.tpjpa.entity.Schedule;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

public class JpaTest {
	private static MeetingDAO meetingDao = EntityManagerHelper.getMeetingDao();
	private static EmployeeDAO employeeDao = EntityManagerHelper.getEmployeeDao();
	private static ScheduleDAO scheduleDao = EntityManagerHelper.getScheduleDao();
	private static DepartmentDAO departmentDao = EntityManagerHelper.getDepartmentDao();


	private EntityManager manager ;
	public JpaTest(EntityManager manager) {
		this.manager = manager;
	}

	public static void main(String[] args) {
		/*
		EntityManagerFactory factory = Persistence.createEntityManagerFactory ("dev");
		EntityManager manager = factory.createEntityManager();
		JpaTest test = new JpaTest(manager);
		EntityTransaction tx = manager.getTransaction();
		tx.begin();
		try {
			test.createEmployees();
		} catch (Exception e) {
			e.printStackTrace();
		}
		tx.commit();
		test.listEmployees();
		manager.close();
		System.out.println(".. done");
		 */

		/*
		Department SI = new Department("SI");
		Department IN = new Department("IN");
		Employee diane = new Employee("diane.sevin@etudiant.univ-rennes1.fr", "Sevin", "Diane", SI);
		Employee caroline = new Employee("caroline.pinte@etudiant.univ-rennes1.fr", "Pinte", "Caroline", IN);

		Meeting meeting1 = new Meeting("TP docker", "Réalisation du Tp docker", diane);
		Schedule choice1 = new Schedule(new Date(), new Date(), "chez moi");
		Schedule choice2 = new Schedule(new Date(), new Date(), "chez toi");
		Schedule choice3 = new Schedule(new Date(), new Date(), "chez soi");
		Schedule choice4 = new Schedule(new Date(), new Date());
		meeting1.addParticipants(caroline);
		meeting1.addChoice(choice1);
		meeting1.addChoice(choice2);
		meeting1.addChoice(choice3);
		meeting1.addChoice(choice4);
		choice3.addVote(caroline);
		choice4.addVote(caroline);

		departmentDao.save(SI);
		departmentDao.save(IN);
		employeeDao.save(diane);
		employeeDao.save(caroline);
		meetingDao.save(meeting1);
		 */
		Department Test = new Department("Test");
		Employee test = new Employee("test@test.com", "Jones", "Jack", Test);
		Employee test1 = new Employee("test1@test.com", "Clack", "Martin", Test);
		Employee test2 = new Employee("test2@test.com", "Miss", "Maria", Test);
		Employee test3 = new Employee("test3@test.com", "Lewis", "Artemis", Test);

		Meeting meeting = new Meeting("Test", "Coordonner les tests", test);
		Schedule choice1 = new Schedule(new Date(), new Date(), "chez moi");
		Schedule choice2 = new Schedule(new Date(), new Date(), "chez toi");
		Schedule choice3 = new Schedule(new Date(), new Date(), "chez soi");
		Schedule choice4 = new Schedule(new Date(), new Date());
		meeting.addParticipants(test1);
		meeting.addParticipants(test2);
		meeting.addParticipants(test3);
		meeting.addChoice(choice1);
		meeting.addChoice(choice2);
		meeting.addChoice(choice3);
		meeting.addChoice(choice4);
		choice3.addVote(test1);
		choice3.addVote(test2);
		choice4.addVote(test3);

		departmentDao.save(Test);
		employeeDao.save(test);
		employeeDao.save(test1);
		employeeDao.save(test2);
		employeeDao.save(test3);
		meetingDao.save(meeting);
	}

	/*
	private void createEmployees() {
		int numOfEmployees = manager.createQuery("Select a From Employee a", Employee.class).getResultList().size();
		if (numOfEmployees == 0) {
			Department department = new Department("java");
			manager.persist(department);
			manager.persist(new Employee("GipszJakab", "Jakab" ,department));
			manager.persist(new Employee("Nemo", "Captain" ,department));
		}
	}
	private void listEmployees() {
		List<Employee> resultList = manager .createQuery("Select a From Employee a", Employee.class).getResultList();
		System.out.println("num of employess:" + resultList.size());
		for (Employee next : resultList) {
			System.out.println("next employee: " + next);
		}
	}
	 */
}