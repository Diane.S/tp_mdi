package fr.istic.tpjpa.dao;


import fr.istic.tpjpa.entity.Meeting;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class MeetingDAO extends Dao<Meeting> {
    public MeetingDAO(EntityManager manager){
        super(manager, Meeting.class);
    }

    public List<Meeting> getAll() {
        Query query = manager.createQuery("select meeting from Meeting meeting", Meeting.class);
        return query.getResultList();
    }

}
