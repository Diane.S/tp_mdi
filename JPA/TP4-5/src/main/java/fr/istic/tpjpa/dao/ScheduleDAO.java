package fr.istic.tpjpa.dao;

import fr.istic.tpjpa.entity.Schedule;

import javax.persistence.EntityManager;

public class ScheduleDAO extends Dao<Schedule> {
        public ScheduleDAO(EntityManager manager){
            super(manager, Schedule.class);
        }
}
