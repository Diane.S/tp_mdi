package fr.istic.tpjpa.entity;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Employee {
	@Id
	@GeneratedValue
	@Column(name = "id")
	private long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "firstname", nullable = false)
	private String firstname;

	@Column(name = "email", unique = true, nullable = false)
	private String email;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "departement_id")
	private Department department;

	@ManyToMany(mappedBy = "participants", fetch = FetchType.LAZY)
	private Set<Meeting> meetings;

	@OneToMany(mappedBy = "organizer", fetch = FetchType.LAZY)
	private Set<Meeting> organizedMeeting;

	public Employee() {
		meetings = new HashSet<Meeting>();
		organizedMeeting = new HashSet<Meeting>();
	}

	public Employee(long id) {
		this();
		this.id = id;
	}

	public Employee(String email, String name, String firstname) {
		this();
		this.email = email;
		this.name = name;
		this.firstname = firstname;
	}

	public Employee(String email, String name, String firstname, Department department) {
		this(email, name, firstname);
		this.department = department;
	}

	@JsonIgnore
	public Set<Meeting> getMeetings() {
		return meetings;
	}

	@JsonIgnore
	public Set<Meeting> getOrganizedMeeting() {
		return organizedMeeting;
	}

	public long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getFirstname() {
		return firstname;
	}

	public Department getDepartment() {
		return department;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@Override
	public boolean equals(Object o){
		if(o==this){
			return true;
		}
		if(!(o instanceof Employee)){
			return false;
		}
		Employee employee =(Employee) o;
		return this.getId()==employee.getId();
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", firstname=" + firstname + ", email=" + email + ", department=" + department.getName() + "]";
	}
}