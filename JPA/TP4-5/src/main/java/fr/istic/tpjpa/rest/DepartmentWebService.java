package fr.istic.tpjpa.rest;

import fr.istic.tpjpa.EntityManagerHelper;
import fr.istic.tpjpa.dao.DepartmentDAO;
import fr.istic.tpjpa.dao.MeetingDAO;
import fr.istic.tpjpa.entity.Department;
import fr.istic.tpjpa.entity.Meeting;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/departments")
public class DepartmentWebService {
    private static DepartmentDAO departmentDao = EntityManagerHelper.getDepartmentDao();

    @GET
    @Path("/{nom}")
    @Produces(MediaType.APPLICATION_JSON)
    public Department getMeeting(@PathParam("nom") String nom) {
        return departmentDao.findbyname(nom);
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Department> getAllQuiz() {
        return departmentDao.getAll();
    }

    @DELETE
    @Path("/{Id}")
    public void deleteDepartment(@PathParam("Id") long Id) {
        try{
            departmentDao.deleteById(Id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }
}
