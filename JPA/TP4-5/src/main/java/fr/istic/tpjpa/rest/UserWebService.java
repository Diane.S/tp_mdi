package fr.istic.tpjpa.rest;

import fr.istic.tpjpa.EntityManagerHelper;
import fr.istic.tpjpa.dao.DepartmentDAO;
import fr.istic.tpjpa.dao.EmployeeDAO;
import fr.istic.tpjpa.entity.Department;
import fr.istic.tpjpa.entity.Employee;
import fr.istic.tpjpa.entity.Meeting;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Set;

@XmlRootElement
class Registration {
    @XmlElement
    public String name;
    @XmlElement
    public String firstname;
    @XmlElement
    public String email;
    @XmlElement
    public String department;
}

@XmlRootElement
class EmployeeUpdate {
    @XmlElement
    public String name;
    @XmlElement
    public String firstname;
    @XmlElement
    public String email;
}

@XmlRootElement
class Connetion {
    @XmlElement public String email;
}

@Path("/users")
public class UserWebService {
    private static EmployeeDAO employeeDao = EntityManagerHelper.getEmployeeDao();
    private static DepartmentDAO departmentDao = EntityManagerHelper.getDepartmentDao();

    @POST
    @Path("/registration")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Employee registration(Registration newEmployee) {
        try {
            Employee employee = new Employee(newEmployee.email, newEmployee.name, newEmployee.firstname);
            employeeDao.save(employee);
            return employee;
        }catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @POST
    @Path("/connection")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Employee connection(Connetion param) {
        try {
            return employeeDao.findbyemail(param.email);
        }catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> getAllEmployee() {
        return employeeDao.getAll();
    }

    @GET
    @Path("{userId}/meetings")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Meeting> getMeetings(@PathParam("userId") long userId) {
       try {
           return employeeDao.findbyId(userId).getMeetings();
       }catch (Exception e) {
           e.printStackTrace();
           throw new WebApplicationException(400);
       }
    }

    @GET
    @Path("{userId}/myMeetings")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Meeting> getMyMeetings(@PathParam("userId") long userId) {
        try {
            return employeeDao.findbyId(userId).getOrganizedMeeting();
        }catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @DELETE
    @Path("/{Id}")
    public void deleteEleve(@PathParam("Id") long id) {
        try{
            employeeDao.deleteById(id);
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{Id}")
    public Employee updateEleve(@PathParam("Id") long id, EmployeeUpdate employee) {
        try{
            Employee e = employeeDao.findbyId(id);
            e.setName((employee.name != null) ? employee.name : e.getName());
            e.setFirstname((employee.firstname != null) ? employee.firstname : e.getFirstname());
            e.setEmail((employee.email != null) ? employee.email : e.getEmail());
            employeeDao.update(e);
            return e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new WebApplicationException(400);
        }
    }
}
