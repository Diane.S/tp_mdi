package fr.istic.tpjpa.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

@Entity
public class Meeting {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "finalSchedule")
    private Schedule finalSchedule;

    @Column(name = "description")
    private String description;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "organizer")
    private Employee organizer;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = "meeting_employee",
            joinColumns = {@JoinColumn(name = "meeting_id")},
            inverseJoinColumns = {@JoinColumn(name = "employee_id")}
    )
    private Set<Employee> participants;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    @JoinColumn(name = "meeting_id")
    private Set<Schedule> choices;

    public Meeting() {
        participants = new HashSet<Employee>();
        choices = new HashSet<Schedule>();
    }

    public Meeting(String name, String description, Employee organizer) {
        this();
        this.name = name;
        this.description = description;
        this.organizer = organizer;
        finalSchedule = null;
        this.organizer.getOrganizedMeeting().add(this);
        addParticipants(organizer);
    }

    public void addParticipants(Set<Employee> enployees) {
        for (Employee e : enployees) {
            addParticipants(e);
        }
    }

    public void addParticipants(Employee employee) {
        participants.add(employee);
        employee.getMeetings().add(this);
    }

    public void removeParticipants(Employee employee){
        assert(participants.contains(employee)&& !organizer.equals(employee));
        participants.remove(employee);

    }
    public void removeChoice(Schedule choice){
        choices.remove(choice);
    }


    public void addChoices(Set<Schedule> choices) {
        choices.addAll(choices);
    }

    public void addChoice(Schedule choice) {
        choices.add(choice);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Employee getOrganizer() {
        return organizer;
    }

    public Set<Employee> getParticipants() {
        return participants;
    }

    public Set<Schedule> getChoices() {
        return choices;
    }

    public Schedule getFinalSchedule(){
        return  finalSchedule;
    }

    public void setFinalSchedule(Schedule schedule){
        finalSchedule = schedule;
    }

    public LinkedList<Schedule> getFinalChoice(){
        int nbVoteMax =0;
        LinkedList<Schedule> finalChoice= new LinkedList<Schedule>();
        for (Schedule choice : choices) {
            int nbVotes= choice.getVotes();
            if(nbVotes>nbVoteMax){
                finalChoice.clear();
                nbVoteMax=nbVotes;
                finalChoice.add(choice);
            }else if(nbVoteMax!=0 && nbVotes==nbVoteMax){
                finalChoice.add(choice);
            }
        }
        return finalChoice;
    }

    public void validate(Schedule schedule) {
        setFinalSchedule(schedule);
    }

    @Override
    public boolean equals(Object o){
        if(o==this){
            return true;
        }
        if(!(o instanceof Employee)){
            return false;
        }
        Employee p =(Employee) o;
        return this.getId()==p.getId();
    }

}
