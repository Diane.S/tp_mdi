package fr.istic.tpjpa;

import fr.istic.tpjpa.dao.DepartmentDAO;
import fr.istic.tpjpa.dao.EmployeeDAO;
import fr.istic.tpjpa.dao.MeetingDAO;
import fr.istic.tpjpa.dao.ScheduleDAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class EntityManagerHelper {

    private static final EntityManagerFactory emf;
    private static final ThreadLocal<EntityManager> threadLocal;
    private static final EmployeeDAO employeeDao ;
    private static final MeetingDAO meetingDao;
    private static final ScheduleDAO scheduleDao;
    private static final DepartmentDAO departmentDao;

    static {
        emf = Persistence.createEntityManagerFactory("dev");
        threadLocal = new ThreadLocal<EntityManager>();
        employeeDao = new EmployeeDAO(getEntityManager());
        meetingDao = new MeetingDAO(getEntityManager());
        scheduleDao = new ScheduleDAO(getEntityManager());
        departmentDao = new DepartmentDAO(getEntityManager());
    }

    public static EntityManager getEntityManager() {
        EntityManager em = threadLocal.get();

        if (em == null) {
            em = emf.createEntityManager();
            threadLocal.set(em);
        }
        return em;
    }

    public static void closeEntityManager() {
        EntityManager em = threadLocal.get();
        if (em != null) {
            em.close();
            threadLocal.set(null);
        }
    }
    public static EmployeeDAO getEmployeeDao(){
        return employeeDao;
    }
    public static MeetingDAO getMeetingDao(){
        return meetingDao;
    }
    public static ScheduleDAO getScheduleDao(){ return scheduleDao; }
    public static DepartmentDAO getDepartmentDao(){return departmentDao;}

    public static void closeEntityManagerFactory() {
        emf.close();
    }

    public static void beginTransaction() {
        getEntityManager().getTransaction().begin();
    }

    public static void rollback() {
        getEntityManager().getTransaction().rollback();
    }

    public static void commit() {
        getEntityManager().getTransaction().commit();
    }
}