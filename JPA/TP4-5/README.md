# Amélioration du TP4 en rajoutant la gestion des réunions en backend et la partie REST

## Objectifs

Pour ce TP je vais apprendre à réaliser une API à l'aide de JPA pour la base de donnée et de Rest pour le partie API réellement. Ce Tp permet vraiment d'appréhender la réalisation de Backend
en Java.

## Comment lancer le Tp

Pour lancer ce Tp il faut à la fois lancé la base de donnée avec `run-hsqldb-server` mais aussi lancer le server avec `jetty:run`. Pour avoir une idée de l'API que j'ai pu réaliser
J'ai mis à la racine aussi de ce Tp un `swagger` qui vous permettra de visualiser ce que mon API peut faire.

## Réalisation

### Réalisation du schéma de la base de donnée
D'après ce qui était demandé dans le sujet de Tp, j'ai pu réaliser une base de donnée de la sorte :
  
<img title="" src="./images/BaseDeDonnees.png" alt="Base de donnée du Tp" data-align="center" width="500">


### Réalisation des entités
A partir du schéma de base de données, j'ai pu réaliser les différentes entités, j'ai eu beaucoup de mal avec la persistence et les cascades.

### Réalisation des DAO
Pour permettre par exemple de rechercher un employé avec son mail, j'ai du définir des DAO et aussi j'ai implémenté des fonctions `getAll()` qui me permet d'avoir tous les élèments
de chaque table.


### Réalisation de la partie REST
J'ai crée trois parties Web service REST qui sont :
* `DepartementWebService` : permet de gérer les départements
* `MeetingWebService`: permet de gérer les réunions
* `UsersWebService`: permet de gérer tout ce qui est en employées (connexion, inscription...)


### Intégration d'un CORSFilter
Le `CORSFilter` permet de gérer les autorisations sur l'API, ici j'ai fait en sorte que mon API soit accessible par d'autre port et non seulement directement sur l'API, pour me 
permettre de réaliser un swagger l'utilisant.

## Problèmes rencontrés
J'ai eu beaucoup de problèmes lié à la persistence de donnée ou des cascades infini que ce créaient entre des élément `@ManyToOne`et `@OneToMany`.
J'ai eu des problèmes aussi lors du peuplement des données et je ne sait pas comment remettre à zero la base de données car il y a des élèments bloquès dedans, auxquels je ne peut
plus toucher.