# TP7 - Réalisation de la partie front, avec Angular, de l'API de sondage réalisé au TP4 et 5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.


## Objectifs

L'objectif de ce Tp est de réaliser un front à l'aide d'angular du backend que nous avons pu réaliser à la fois dans le TP4 et le TP5. POur faire cela je me suis beaucoup inspiré
du Tp d'initiation à Angular que nous avons fait précédement avec la `PokeAPI`.  
  
<img title="" src="./images/4.png" alt="Aperçu de l'API de sondage" data-align="center" width="500">


## Comment lancer le Tp

Avant cela, lancer le TP4-5 et le serveur de base de données pour que le 4200 puisse se connecter au 8080.  
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Pour peupler la base vous pouvez lancer JpaTest du TP4-5.


## Réalisation

Tout d'abord par rapport au Tp sur le Pokedex, j'ai utilisé un router pour permettre de switch entre deux composants et j'ai aussi créer un service à la fois rest mais aussi un service qui
me permettant de gérer la connexion.
  
Le service web que j'ai mis en place permet de s'inscrire et de s'identifier afin de créer et visualiser des réunions.  
  
  <img title="" src="./images/1.png" alt="" data-align="center" width="500">  
  
  On peut ensuite voir quand on se connecte, les réunions que l'on à avec tous les détails et les choix que l'on peut faire pour la date.  
  
  
<img title="" src="./images/2.png" alt="" data-align="center" width="500">  
  
On peut aussi créer des meetings à l'aide du service en choississant les créneaux (schedule) qui vont s'ajouter visuellement en dessous pour garder un oeil sur les ajouts.
Pour les participants, j'ai rajouté l'option pour les supprimer, mais vous ne pourrez jamais supprimer la personne connecté qui est l'organisateur. Et vous pouvez de même voir la liste
en dessous.  
  
<img title="" src="./images/3.png" alt="" data-align="center" width="500">  
  
Vous pouvez aussi comme vous le voyez supprimer un vote ou en ajouter d'autres.  
  
<img title="" src="./images/5.png" alt="" data-align="center" width="500">  

## Problèmes rencontrés

J'ai eu beaucoup de problème avec le CORSFilter, je n'avais pas toujours les droits pour accéder à mon API alors qu'elle est ouverte pour tout port, toute méthodes... Je n'avais pas eu
de problèmes pourtant avec le `swagger` du TP4-5 mais là j'en ai eu.