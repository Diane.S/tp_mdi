import { Component, OnInit } from '@angular/core';
import { RestService } from '../../services/rest.service';
import {ConnectionService} from '../../services/connection.service';
import {Router} from '@angular/router';
import {Department} from '../../class/department';

@Component({
  selector: 'app-login',
  templateUrl: './login-manager.component.html',
  styleUrls: ['./login-manager.component.css']
})
export class LoginManagerComponent implements OnInit {
  email: string;
  nom: string;
  prenom: string;
  department: string;
  departments: Department[];


  constructor(public rest: RestService, public state: ConnectionService, public router: Router) { }

  ngOnInit(): void {
    this.rest.getDepartments().subscribe(data => this.departments = data);
  }

  connexion(): void {
    this.rest.connexion(this.email).subscribe(
      user => {
        this.state.login(user);
        this.router.navigate(['/meeting-manager']);
    }, error => {
        alert('Adresse mail invalide !');
    });
    return;
  }

  inscription(): void {
    this.rest.addUser(this.nom, this.prenom, this.email).subscribe(
      user => {
        alert('Inscription faite !');
      }, error => {
        alert('Inscription impossible !');
      });
    return;
  }

}
