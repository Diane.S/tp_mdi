import { Component, OnInit } from '@angular/core';
import {ConnectionService} from '../../services/connection.service';
import {Meeting} from '../../class/meeting';
import {RestService} from '../../services/rest.service';
import {Employee} from '../../class/employee';
import {Schedule} from '../../class/schedule';
import {ScheduleTemp} from '../../class/scheduleTemp';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting-manager.component.html',
  styleUrls: ['./meeting-manager.component.css']
})
export class MeetingManagerComponent implements OnInit {
  meetings: Array<Meeting>;
  name: string;
  description: string;
  employee: Employee;
  employees: Employee[];
  email: string;
  participants: Employee[] = [];
  dateDebut: Date;
  dateFin: Date;
  schedules: ScheduleTemp[] = [];
  location = '';

  constructor(public state: ConnectionService, public rest: RestService) {
    this.meetings = new Array<Meeting>();
  }

  ngOnInit(): void {
    this.employee = this.state.getUser();
    this.rest.getMeeting(this.state.getUser().id).subscribe(meetings => {
      this.meetings = meetings;
    });
    this.rest.getEmployees().subscribe(employees => {
      this.employees = employees;
    });
    this.participants.push(this.state.getUser());
  }

  addMeeting(): void {
    this.rest.addMeeting(this.name, this.description, this.state.getUser().id).subscribe(meeting => {
      this.rest.addChoices(meeting.id, this.schedules);
      this.rest.addParticipants(meeting.id, this.participants);
      this.meetings.push(meeting);
    });
  }

  contains(u: Employee, a: Array<Employee>): boolean{
    if (a){
      for (const user of a){
        if (user.id === u.id){
          return true;
        }
      }
      return false;
    }
    console.log('nop');
    return false;
}
  addVote(meeting: Meeting, schedule: Schedule){
    console.log(schedule);
    this.rest.addVote(meeting.id, schedule.id, this.employee.id).subscribe(m => {
      this.meetings = this.meetings.filter(obj => obj.id !== m.id);
      this.meetings.push(m); });
  }

  deleteVote(meeting: Meeting, schedule: Schedule){
    this.rest.deleteVote(meeting.id, schedule.id, this.employee.id).subscribe(m => {
      this.meetings = this.meetings.filter(obj => obj.id !== m.id);
      this.meetings.push(m);
    });
  }

  addParticipant(): void {
    this.rest.connexion(this.email).subscribe(
      user => {
        for (const p of this.participants){
          if (p.email === user.email){
            return;
          }
        }
        this.participants.push(user);
      });
    return;
  }

  deleteParticipant(){
    this.rest.connexion(this.email).subscribe(
      user => {
        let i = 0;
        for (const p of this.participants){
          if (p.email === user.email){
            if (p.id === this.state.getUser().id){
              return;
            }
            this.participants.splice(i, 1);
            return;
          }
          i++;
        }
      });
  }

  addCreneau(){
    console.log(new ScheduleTemp(this.dateDebut, this.dateFin, this.location));
    this.schedules.push(new ScheduleTemp(this.dateDebut, this.dateFin, this.location));
  }

  printLocation(location: String){
    if (location != "" && location != null){
      return("Location: "+location);
    }
    return "";
  }
}
