import { LoginManagerComponent } from './components/login-manager/login-manager.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MeetingManagerComponent} from './components/meeting-manager/meeting-manager.component';
import {LoginGuard} from './class/login.guard';


const routes: Routes = [
  { path: '', component: LoginManagerComponent },
  { path: 'meeting-manager', component: MeetingManagerComponent, canActivate: [LoginGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
