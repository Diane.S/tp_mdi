import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginManagerComponent } from './components/login-manager/login-manager.component';
import {FormsModule} from '@angular/forms';
import {MeetingManagerComponent} from './components/meeting-manager/meeting-manager.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginManagerComponent,
    MeetingManagerComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
