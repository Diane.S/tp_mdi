export class ScheduleTemp{
  end: Date;
  start: Date;
  location: string;

  constructor(start: Date, end: Date, location: string ){
    this.end = end;
    this.start = start;
    this.location = location;
  }
}
