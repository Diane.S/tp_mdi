import {Employee} from './employee';

export class Schedule{
  id: number;
  end: Date;
  start: Date;
  location: string;
  votes: Array<Employee>;

  constructor(data: any){
    this.id = data.id;
    this.end = new Date(data.end);
    this.start = new Date(data.start);
    this.location = data.location;
    this.votes = new Array<Employee>();
    for (const e of data.employees) {
      this.votes.push(new Employee(e));
    }
  }
}
