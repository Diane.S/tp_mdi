export class Employee {
  constructor(res: any) {
    this.id = res.id;
    this.email = res.email;
    this.name = res.name;
    this.firstname = res.firstname;
  }

  id: number;
  email: string;
  name: string;
  firstname: string;
}
