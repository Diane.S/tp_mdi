import {Employee} from './employee';
import {Schedule} from './schedule';

export class Meeting {
  id: number;
  name: string;
  description: string;
  organizer: Employee;
  participants: Array<Employee>;
  choices: Array<Schedule>;

  constructor(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.organizer = new Employee(data.organizer);
    this.participants = new Array<Employee>();
    for (const e of data.participants) {
      this.participants.push(new Employee(e));
    }
    this.choices = new Array<Schedule>();
    for (const h of data.choices) {
      this.choices.push(new Schedule(h));
    }
  }
}
