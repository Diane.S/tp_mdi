import { TestBed } from '@angular/core/testing';

import { ConnectionService } from './connection.service';

describe('StateService', () => {
  let service: ConnectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
