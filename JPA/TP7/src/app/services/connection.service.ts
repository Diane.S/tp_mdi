import { Injectable } from '@angular/core';
import {Employee} from '../class/employee';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  private user: Employee;

  constructor(public router: Router) {
    this.user = JSON.parse(localStorage.getItem('user'));
  }

  login(user: Employee): void {
    this.user = user;
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  logout(): void {
    this.user = null;
    localStorage.removeItem('user');
    this.router.navigate(['/']);
  }

  isLogged(): boolean {
    return this.user !== null;
  }

  getUser(): Employee {
    return this.user;
  }

}
