import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {Employee} from '../class/employee';
import {map} from 'rxjs/operators';
import {Meeting} from '../class/meeting';
import {Department} from '../class/department';
import {Schedule} from '../class/schedule';
import {ScheduleTemp} from '../class/scheduleTemp';

const endpoint = 'http://localhost:8080/rest/';
const httpOptions = {
  headers: new HttpHeaders({
    'Access-Control-Allow-Headers':'*',
    'Access-Control-Allow-Origin':'*',
    'Access-Control-Allow-Credentials':'true',
    'Access-Control-Allow-Methods':'GET, POST, PUT, DELETE, OPTIONS, HEAD',
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  connexion(e: string): Observable<Employee> {
    return this.http.post<Employee>(endpoint + 'users/connection', JSON.stringify({email: e}), httpOptions)
      .pipe(map(res => new Employee(res)));
  }

  getMeeting(id: number): Observable<Array<Meeting>>{
    return this.http.get<Array<Meeting>>(endpoint + 'users/' + id + '/meetings', httpOptions)
      .pipe(map(res => {
        const meetings = new Array<Meeting>();
        for (const r of res) {
          meetings.push(new Meeting(r));
        }
        return meetings;
      }));
  }

  addMeeting(name: string, description: string, organizer: number): Observable<Meeting>{
    return this.http.post<Meeting>(endpoint + 'meetings', JSON.stringify({name, description, organizer}), httpOptions)
    .pipe(map(res => new Meeting(res)));
  }

  addVote(meetingId: number, horaireId: number, userId: number){
    console.log(endpoint + 'meetings/' + meetingId + '/schedule/' + horaireId + '/vote' + userId);
    return this.http.post<Meeting>(endpoint + 'meetings/' + meetingId + '/schedule/' + horaireId
      + '/voter', JSON.stringify({id: userId}), httpOptions )
      .pipe(map(res => new Meeting(res)));
  }

  deleteVote(meetingId: number, horaireId: number, userId: number){
    console.log(endpoint + 'meetings/' + meetingId + '/schedule/' + horaireId + '/vote' + userId);
    return this.http.delete<Meeting>(endpoint + 'meetings/' + meetingId + '/schedule/' + horaireId
      + '/participants/' + userId + '/vote', httpOptions )
      .pipe(map(res => new Meeting(res)));
  }

  addUser(name: string, firstname: string, email: string){
    console.log(JSON.stringify({name, firstname, email}));
    return this.http.post<Meeting>(endpoint + 'users', JSON.stringify({name, firstname, email}), httpOptions)
      .pipe(map(res => new Meeting(res)));
  }

  getDepartments(){
    return this.http.get<Array<Department>>(endpoint + 'departments/', httpOptions);
  }

  getEmployees(){
    return this.http.get<Array<Employee>>(endpoint + 'users/', httpOptions);
  }

  addChoices(id: number, choices: ScheduleTemp[]){
    for (const choice of choices){
      console.log(JSON.stringify({start: choice.start, end: choice.end, location: choice.location}));
      this.http.post<Schedule>(endpoint + 'meetings/' + id + '/choices', JSON.stringify({start: choice.start, end: choice.end, location: choice.location}), httpOptions).pipe(map(res => new Meeting(res)));
    }
  }

  addParticipants(id: number, participants: Employee[]){
    for (const employee of participants){
      this.http.post<Employee>(endpoint + 'meetings/' + id + '/participants', JSON.stringify({id: employee.id}), httpOptions).pipe(map(res => new Meeting(res)));
    }
  }
}
