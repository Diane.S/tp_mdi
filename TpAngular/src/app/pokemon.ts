export interface PokemonServiceResults {
  count: number;
  next: string;
  previous: null;
  results: InterfacePokemons[];
}

export interface InterfacePokemons {
  name: string;
  url: string;
}

export interface PokemonDetails {
  abilities: Ability[];
  base_experience: number;
  forms: Species[];
  game_indices: GameIndex[];
  height: number;
  held_items: HeldItem[];
  id: number;
  is_default: boolean;
  location_area_encounters: string;
  moves: Move[];
  name: string;
  order: number;
  species: Species;
  sprites: Sprites;
  stats: Stat[];
  types: Type[];
  weight: number;
}

export interface Ability {
  ability: Species;
  is_hidden: boolean;
  slot: number;
}

export interface Species {
  name: string;
  url: string;
}

export interface GameIndex {
  game_index: number;
  version: Species;
}

export interface HeldItem {
  item: Species;
  version_details: VersionDetail[];
}

export interface VersionDetail {
  rarity: number;
  version: Species;
}

export interface Move {
  move: Species;
  version_group_details: VersionGroupDetail[];
}

export interface VersionGroupDetail {
  level_learned_at: number;
  move_learn_method: Species;
  version_group: Species;
}

export interface Sprites {
  back_default: string;
  back_female: string;
  back_shiny: string;
  back_shiny_female: string;
  front_default: string;
  front_female: string;
  front_shiny: string;
  front_shiny_female: string;
}

export interface Stat {
  base_stat: number;
  effort: number;
  stat: Species;
}

export interface Type {
  slot: number;
  type: Species;
}

export interface PokemonZones {
  location_area: LocationArea;
  version_details: VersionDetail[];
}

export interface LocationArea {
  name: string;
  url: string;
}

export interface VersionDetail {
  encounter_details: EncounterDetail[];
  max_chance: number;
  version: LocationArea;
}

export interface EncounterDetail {
  chance: number;
  condition_values: LocationArea[];
  max_level: number;
  method: LocationArea;
  min_level: number;
}



export class Pokemon {

  constructor(public id: string, public nom: string, public url: string) {
  }

}
