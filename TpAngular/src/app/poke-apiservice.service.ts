import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PokemonDetails, PokemonServiceResults, PokemonZones} from './pokemon';

const url = 'https://pokeapi.co/api/v2/pokemon/';

@Injectable({
  providedIn: 'root'
})

export class PokeAPIServiceService {
  constructor(private http: HttpClient) {}

  getPokemons(): Observable<PokemonServiceResults>{
    return this.http.get<PokemonServiceResults>(url + '?offset=0&limit=807/');
  }

  getPokemonInfoByID(id: string): Observable<PokemonDetails>{
    return this.http.get<PokemonDetails>(url + id + '/');
  }

  getPokemonInfoByName(name: string): Observable<PokemonDetails>{
    return this.http.get<PokemonDetails>(url + name + '/');
  }

  getPokemonZones(id: string): Observable<PokemonZones>{
    return this.http.get<PokemonZones>(url + id + '/encounters/');
  }
}
