import { Component, OnInit } from '@angular/core';
import {Pokemon, PokemonDetails, PokemonZones} from '../pokemon';
import {PokeAPIServiceService} from '../poke-apiservice.service';

export interface Types{
  nom: string;
  url: string;
}

@Component({
  selector: 'app-my-component',
  templateUrl: './my-component.component.html',
  styleUrls: ['./my-component.component.css'],
  providers: [PokeAPIServiceService]
})
export class MyComponentComponent implements OnInit {
  id = '';

  pokemons: Pokemon[] = [];
  rechercheParNom = '';
  pokemonSelectionne = '';
  pokemonDetails: PokemonDetails;
  pokemonZones: PokemonZones;
  jeuSelectionne = '';
  versions = ['red-blue', 'yellow', 'gold-silver', 'crystal', 'ruby-sapphire', 'colosseum', 'emerald', 'firered-leafgreen', 'xd', 'diamond-pearl', 'platinum', 'heartgold-soulsilver', 'black-white', 'black-2-white-2', 'x-y', 'omega-ruby-alpha-sapphire', 'sun-moon', 'ultra-sun-ultra-moon'];
  listeItems: string[] = [];
  jeuItems = '';
  listeAttaquesLvl: string[] = [];
  jeuAttaquesLvl = '';
  listeAttaquesMachine: string[] = [];
  listeAttaquesTutor: string[] = [];
  listeAttaquesEgg: string[] = [];
  listeAttaquesAutre: string[] = [];
  jeuAttaquesAutre = '';


  TypeNoms: Types[] = [
    {nom: 'steel', url: 'https://www.pokebip.com/pokedex-images/types/acier.png'},
    {nom: 'fighting', url: 'https://www.pokebip.com/pokedex-images/types/combat.png'},
    {nom: 'dragon', url: 'https://www.pokebip.com/pokedex-images/types/dragon.png'},
    {nom: 'water', url: 'https://www.pokebip.com/pokedex-images/types/eau.png'},
    {nom: 'electric', url: 'https://www.pokebip.com/pokedex-images/types/electrik.png'},
    {nom: 'fairy', url: 'https://www.pokebip.com/pokedex-images/types/fee.png'},
    {nom: 'fire', url: 'https://www.pokebip.com/pokedex-images/types/feu.png'},
    {nom: 'ice', url: 'https://www.pokebip.com/pokedex-images/types/glace.png'},
    {nom: 'bug', url: 'https://www.pokebip.com/pokedex-images/types/incecte.png'},
    {nom: 'normal', url: 'https://www.pokebip.com/pokedex-images/types/normal.png'},
    {nom: 'grass', url: 'https://www.pokebip.com/pokedex-images/types/plante.png'},
    {nom: 'poison', url: 'https://www.pokebip.com/pokedex-images/types/poison.png'},
    {nom: 'psychic', url: 'https://www.pokebip.com/pokedex-images/types/psy.png'},
    {nom: 'rock', url: 'https://www.pokebip.com/pokedex-images/types/roche.png'},
    {nom: 'ground', url: 'https://www.pokebip.com/pokedex-images/types/sol.png'},
    {nom: 'ghost', url: 'https://www.pokebip.com/pokedex-images/types/spectre.png'},
    {nom: 'dark', url: 'https://www.pokebip.com/pokedex-images/types/tenebres.png'},
    {nom: 'flying', url: 'https://www.pokebip.com/pokedex-images/types/vol.png'}
  ];

  constructor(private pokeAPIService: PokeAPIServiceService) {
    /*
    this.pokemons.push(new Pokemon('001', 'Ouistempo'));
    this.pokemons.push(new Pokemon('002', 'Badabouin'));
    this.pokemons.push(new Pokemon('003', 'Gorythmic'));
    this.pokemons.push(new Pokemon('004', 'Flambino'));
    this.pokemons.push(new Pokemon('005', 'Lapyro'));
    this.pokemons.push(new Pokemon('006', 'Pyrobut'));
    this.pokemons.push(new Pokemon('007', 'Larméléon'));
    this.pokemons.push(new Pokemon('008', 'Arrozard'));
    this.pokemons.push(new Pokemon('009', 'Lézargus'));
     */
  }

  ngOnInit(): void {
    this.pokeAPIService.getPokemons().subscribe((data => {
      console.log(data);
      data.results.forEach((e, index) => {
        const indextrue = index + 1;
        this.pokemons.push(new Pokemon('' + indextrue, e.name, e.url));
      });
    }));
  }

  go1(){
    if (this.id !== ''){
      this.pokeAPIService.getPokemonInfoByID(this.id).subscribe(data => this.pokemonDetails = data);
      this.jeuSelectionne = '';
    }
  }

  go2(){
    if (this.pokemonSelectionne !== ''){
        this.pokeAPIService.getPokemonInfoByID(this.pokemonSelectionne).subscribe(data => this.pokemonDetails = data);
        this.pokemonSelectionne = '';
        this.jeuSelectionne = '';
      }
    else if (this.rechercheParNom !== ''){
      this.pokeAPIService.getPokemonInfoByName(this.rechercheParNom.toLowerCase()).subscribe(data => this.pokemonDetails = data);
      this.pokemonSelectionne = '';
      this.jeuSelectionne = '';
    }
  }

  zones(){
    this.pokeAPIService.getPokemonZones(this.pokemonDetails.id.toString()).subscribe(data => this.pokemonZones = data);
  }

  mettreDesZeros(id: string){
    if (id.length < 3){
      if (id.length === 1){
        return '00' + id;
      }
      else if (id.length === 2){
        return '0' + id;
      }
    }
    else {
      return (id);
    }
  }

  isFemale(){
    if (this.pokemonDetails.sprites.front_female !== null){
      return this.pokemonDetails.sprites.front_female;
    }
    else {
      return this.pokemonDetails.sprites.front_default;
    }
  }

  isFemaleShiny(){
    if (this.pokemonDetails.sprites.front_shiny_female !== null){
      return this.pokemonDetails.sprites.front_shiny_female;
    }
    else {
      return this.pokemonDetails.sprites.front_shiny;
    }
  }

  whatType(type: string){
    let index = 0;
    while (this.TypeNoms[index].nom !== type){
      index++;
    }
    return this.TypeNoms[index].url;
  }

  convert(value: string){
    if (value.length === 1){
      return ('0.' + value.charAt(0));
    }
    else {
      return (value.substr(0, value.length - 1) + '.' + value.charAt(value.length - 1));
    }
  }

  creerListeItems(){
    if (this.jeuSelectionne === ''){
      this.listeItems = [];
    }
    else if (this.jeuSelectionne !== this.jeuItems){
      this.listeItems = [];
      for (const item of this.pokemonDetails.held_items) {
        for (const detail of item.version_details) {
          if (detail.version.name === this.jeuSelectionne) {
            this.listeItems.push(item.item.name + ' (' + detail.rarity + '%)');
          }
        }
      }
      this.jeuItems = this.jeuSelectionne;
    }
  }

  creerListeAttaquesLvl(){
    if (this.jeuSelectionne === ''){
      this.listeAttaquesLvl = [];
    }
    else if (this.jeuSelectionne !== this.jeuAttaquesLvl) {
      this.listeAttaquesLvl = [];
      for (const move of this.pokemonDetails.moves) {
        for (const detail of move.version_group_details) {
          if (detail.version_group.name.includes(this.jeuSelectionne) && detail.level_learned_at !== 0) {
            this.listeAttaquesLvl.push(move.move.name + ' apprise au niveau ' + detail.level_learned_at + '');
          }
        }
      }
      this.jeuAttaquesLvl = this.jeuSelectionne;
    }
  }

  creerListeAttaquesAutres(){
    if (this.jeuSelectionne === ''){
      this.listeAttaquesMachine = [];
      this.listeAttaquesTutor = [];
      this.listeAttaquesEgg = [];
      this.listeAttaquesAutre = [];
    }
    else if (this.jeuSelectionne !== this.jeuAttaquesAutre) {
      this.listeAttaquesMachine = [];
      this.listeAttaquesTutor = [];
      this.listeAttaquesEgg = [];
      this.listeAttaquesAutre = [];
      for (const move of this.pokemonDetails.moves) {
        for (const detail of move.version_group_details) {
          if (detail.version_group.name.includes(this.jeuSelectionne) && detail.level_learned_at === 0) {
            if (detail.move_learn_method.name === 'machine') {
              this.listeAttaquesMachine.push(move.move.name);
            } else if (detail.move_learn_method.name === 'tutor') {
              this.listeAttaquesTutor.push(move.move.name);
            } else if (detail.move_learn_method.name === 'egg') {
              this.listeAttaquesEgg.push(move.move.name);
            } else {
              this.listeAttaquesAutre.push(move.move.name + ' apprise avec la méthode ' + detail.move_learn_method.name);
            }
          }
        }
      }
      this.jeuAttaquesAutre = this.jeuSelectionne;
    }
  }

  nombreColonnes(){
    let result = 0;
    if (this.listeAttaquesLvl.length !== 0){
      result++;
    }
    if (this.listeAttaquesMachine.length !== 0){
      result++;
    }
    if (this.listeAttaquesTutor.length !== 0){
      result++;
    }
    if (this.listeAttaquesEgg.length !== 0){
      result++;
    }
    if (this.listeAttaquesAutre.length !== 0){
      result++;
    }
    return result;
  }
}
