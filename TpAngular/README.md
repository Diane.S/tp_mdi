# TpAngular

J'ai généré ce projet à l'aide de [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## Lancer le server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Objectif de ce Tp

Ce Tp a pour objectif de m'aider à découvrir angular à l'aide d'une API très connue et très utilisée qui est `https://pokeapi.co/`. Cette API va me permettre durant ce Tp
de réaliser un Pokedex, un endroit où on peut retrouver de multiples informations sur tous les pokemons, ici que jusqu'à la 7ème génération. Voici un aperçu de ce que j'ai
pu réaliser :

<img title="" src="./images/1.png" alt="Aperçu du Pokedex" data-align="center" width="500">


## Initiation à angular

J'ai commencé par réaliser tout ce qui était demander au sein du Tp puis je suis allée plus loin car je voulais voir les possibilités que pouvait offrir Angular.
J'ai ainsi pu voir comment on pouvait réaliser des composant, ou comment se servir d'une API pour afficher tels ou tels élèment qu'elle contient.
J'ai pu utiliser des filtres ainsi que des controleurs par exemple. Puis à l'aide de tout ça j'ai intégrer une partie graphique.

## Réalisation d'un pokedex le plus complet possible

Tout d'abord, nous pouvons grâce à ce que j'ai pu voir dans le sujet de Tp sélectionner à l'aide d'un id un pokemon de l'API.  

<img title="" src="./images/2.png" alt="Selection by Id" data-align="center" width="500">  
  
Par la suite nous pouvons selectionner parmis une liste le pokemon que nous souhaitons afficher, ici il y a besoin dun filtre pour que ce que nous écrivions corresponde au pokemons
présents dans la liste. Au début je ne savais pas comment supprimer la limite des 20 pokemons pour que cette liste puisse être complète. En modifiant les contrainte dans l'url
j'ai pu faire fonctionner la liste comme je le voulais.

<img title="" src="./images/3.png" alt="Selection by Name" data-align="center" width="500">  
  
Puis lorsque je selectionne par exemple pikachu (en cherchant les pokemons contenant un "pi" dans le nom), on peut ainsi y voir une image du pikachu mais aussi d'autres informations :
* Son identifiant
* Son ordre
* Sa taille
* Son poids
* Son expérience de base
* Ses ou son type(s)
* Différentes statistiques (hp, vitesse, ...)
* Ses abilités
* Ses différentes formes
* Un lien vers les zones où on peut les rencontrer (je n'ai pas pu utiliser correctement l'API pour afficher les différentes zones il manquait un champs pour que je puisse)
* Puis d'autres informations supplémentaire (que je vais vous détailler par la suite)

  
<img title="" src="./images/4.png" alt="Aperçu d'un pokemon 1" data-align="center" width="500">  
  
Nous pouvons voir que nous pouvons sélectionner différents jeux et ainsi acceder aux informations lié à ces jeux car par exemple la présente d'items dépend du jeux mais les
attaques dépendent aussi des jeux et versions mais aussi de comment les apprendre.

<img title="" src="./images/5.png" alt="Appercu d'un pokemon 2" data-align="center" width="500">  <img title="" src="./images/6.png" alt="Aperçu d'un pokemon 3" data-align="center" width="500">  
  
La liste des jeux où le pokemon étant présent dans l'API ne correspondait pas exactement aux informations sur les attaques par exemple les pokemons qui était de la génération 6 n'avait
aucun jeu où il apparaissaient alors qu'ils avaient des attaques lié aux jeux de 6ème génération. Ainsi j'ai fait une liste de toutes les versions disponibles sur l'API et quand ils
n'avaient pas d'attaques associé à la version cela voulait dire qu'il n'y était pas présent ainsi j'affiche le message que vous pouvez voir dans cette image suivante.

<img title="" src="./images/7.png" alt="Pokemon qui est présent que sur certains jeux" data-align="center" width="500">  
  
Cette API a des limites car elle ne contient pas les pokemons de 8ème génération après le numéro 807 ainsi c'est le dernier qui se trouve sur l'image qui suit.  

<img title="" src="./images/8.png" alt="Pokemon avec l'Id la plus grande" data-align="center" width="500">  
  
Lors de la réalisation de mon Pokedex, l'API ne contenait pas d'images pour les pokemons entre 804 et 807, mais j'ai dernièrement relancé et maintenant elle l'est prenant en compte à
ma grande surprise car cela me genait que les images s'affiche pas car j'aime quand c'est parfait surtout sur des choses que j'ai aimé découvrir et faire.



## Ce que j'aurais voulu faire mais que je n'ai pas pu

J'aurais voulu inclure les différentes zones où on peut trouver les pokemons en fonctions de leurs version mais l'API ne m'a pas permis de le faire malheureusement. J'ai cherché pendant 
longtemps et j'ai décidé de laisser avec juste une url car sinon j'allais perdre peut être encore beaucoup de temps aors qu'il n'y a peut-être pas de solution.